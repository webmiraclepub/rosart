<div style="background-image: url(<?= $bgi_lazy ?>); background-position: center; background-size: cover;">
    <div class="about-us" data-src="<?= $bgi_full ?>">
        <div class="about-us__container">
            <div class="about-us__info send-app">
                <h3 class="send-app__title"><?= $title ?></h3>
                <p class="send-app__subtitle"><?= $subtitle ?></p>
                <p class="send-app__content"><?= $content ?></p>
                <button class="about-us__button miracle-button" data-modal="miracle-modal-send-list" type="button" name="button">Оставить заявку</button>
            </div>
            <img class="about-us__image about-us_hide-mobile about-us__image_pull-down" src="<?= $image_full ?>"  data-src="<?= $image_lazy ?>" alt="<?= $image_alt ?>" title="<?= $image_title ?>">
        </div>
    </div>
</div>
