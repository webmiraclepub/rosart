<?php if( is_page_template( 'pagetemplates/contacts.php' ) ): ?>
<div id="yandex-map" class="yandex-map" data-page="contacts"></div>
<?php else: ?>
<div id="yandex-map" class="yandex-map"></div>
<?php endif; ?>

<script type="text/javascript">
    var miracleYandex = {
        x : <?= $x ?>,
        y: <?= $y ?>,
        xc : <?= $center_x ?>,
        yc: <?= $center_y ?>,
        hint: '<?= $hint ?>',
        ballon: "<?= $ballon ?>",
        icon: '<?= $image ?>'
    }
</script>
