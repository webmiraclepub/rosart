<div class="miracle-modal" data-modal="miracle-modal-send-phone" data-src="<% bgi-full %>">
    <div class="miracle-modal__send-form send-form">
        <form>
            <h5 class="send-form__title send-form__title_text-left send-form__title_font-big"><% title %></h5>
            <p class="send-form__subtitle send-form__subtitle_text-left send-form__subtitle_text-gray send-form__subtitle_font-big"><% subtitle %></p>
            <div class="send-form__input-block">
                <label for="f2а">
                    <p class="send-form__label send-form__label_color-gray">Имя</p>
                </label>
                <input class="send-form__input send-form__input_color-gray" type="text" name="name" id="f2а" value="" autocomplete="off"  required="required"/>
            </div>
            <div class="send-form__input-block">
                <label for="f12а">
                    <p class="send-form__label send-form__label_color-gray">Телефон</p>
                </label>
                <input class="send-form__input send-form__input_color-gray" type="tel" name="telephone" id="f12а" value="" autocomplete="off"  required="required"/>
            </div>
            <div class="send-form__submit send-form__submit_modal">
                <input type="hidden" name="action" value="send-phone">
                <button type="submit" class="miracle-button">Отправить</button>
            </div>
        </form>
    </div>
    <button class="miracle-modal__close-button miracle-font-cross" type="button" name="button"></button>
</div>
