<div class="bottom-form bottom-form_hide-medium">
    <div class="bottom-form__content">
        <div class="bottom-form__send-form send-form">
            <form>
                <h5 class="send-form__title">Оставьте заявку</h5>
                <p class="send-form__subtitle">И наши специалисты свяжутся с вами в самое ближайшее время</p>
                <div class="send-form__input-block">
                    <label for="f2">
                        <p class="send-form__label">Имя</p>
                    </label>
                    <input class="send-form__input" type="text" name="name" id="f2" value="" autocomplete="off" required="required"/>
                </div>
                <div class="send-form__input-block">
                    <label for="f12">
                        <p class="send-form__label">Телефон</p>
                    </label>
                    <input class="send-form__input" type="tel" name="telepfone" id="f12" value="" autocomplete="off" required="required"/>
                </div>
                <div class="send-form__input-block">
                    <label for="f32">
                        <p class="send-form__label">E-mail</p>
                    </label>
                    <input class="send-form__input" type="email" name="email" id="f32" value="" autocomplete="off" required="required"/>
                </div>
                <div class="send-form__input-block">
                    <label for="f323">
                        <p class="send-form__label">Ваш комментарий</p>
                    </label>
                    <input class="send-form__input" type="text" name="comment" id="f323" value="" autocomplete="off" required="required"/>
                </div>
                <div class="send-form__submit">
                    <input type="hidden" name="action" value="send-list-form">
                    <button type="submit" class="miracle-button">Отправить</button>
                </div>
            </form>
        </div>
    </div>
</div>
