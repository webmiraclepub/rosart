<div class="work-step" data-src="<?= $theme_dir_uri ?>/images/10section.png">
    <div class="work-step__content">
        <h2 class="work-step__block-title miracle-title">Как мы работаем</h2>
        <div class="work-step__step-card step-card">
            <div class="step-card__block">
                <div class="step-card__icon" style="background-color: #ff4e52"><i class="miracle-font-brif2"></i></div>
                <h3 class="step-card__title">1. Заполнение <br>брифа</h3>
                <p class="step-card__content">Заполнение клиентом <br>брифа для получения нами <br>общей информации</p>
            </div>
            <div class="step-card__block">
                <div class="step-card__icon" style="background-color: #2898e0"><i class="miracle-font-dogovor"></i></div>
                <h3 class="step-card__title">2. Заключение <br>договора</h3>
                <p class="step-card__content">После заполнения брифа и <br>составления технического <br>задания, мы заключаем с вами <br>договор</p>
            </div>
            <div class="step-card__block">
                <div class="step-card__icon" style="background-color: #34bf5f"><i class="miracle-font-prototype"></i></div>
                <h3 class="step-card__title">3. Создание <br>прототипа</h3>
                <p class="step-card__content">Разработка интерактивного <br>прототипа и согласование <br>общей структуры</p>
            </div>
            <div class="step-card__block">
                <div class="step-card__icon" style="background-color: #f0d127"><i class="miracle-font-design2"></i></div>
                <h3 class="step-card__title">4. Разработка <br>дизайна</h3>
                <p class="step-card__content">Создание уникального и <br>эксклюзивного дизайна, и <br>утверждение его клиентом</p>
            </div>
            <div class="step-card__block">
                <div class="step-card__icon" style="background-color: #c058b5"><i class="miracle-font-html"></i></div>
                <h3 class="step-card__title">5. HTML-верстка</h3>
                <p class="step-card__content">HTML-верстка дизайн-макетов <br>и предоставление клиенту <br>архива с готовыми <br>html-страницами</p>
            </div>
            <div class="step-card__block">
                <div class="step-card__icon" style="background-color: #20dcbb"><i class="miracle-font-programming"></i></div>
                <h3 class="step-card__title">6. Программирование</h3>
                <p class="step-card__content">Привязка html-страниц к системе <br>управления сайтом. Мы <br>используем только <br>общедоступные CMS, и вы сами <br>легко сможете менять контент на <br>сайте</p>
            </div>
            <div class="step-card__block">
                <div class="step-card__icon" style="background-color: #f3a2b9"><i class="miracle-font-promotion"></i></div>
                <h3 class="step-card__title">7. Продвижение и <br>рекламма</h3>
                <p class="step-card__content">Мы можем предложить услуги <br>по продвижению сайта в <br>поисковых системах, социальных <br>сетях, а также настройке <br>контекстной и таргетированной <br>рекламы</p>
            </div>
            <div class="step-card__block">
                <div class="step-card__icon" style="background-color: #7adeff"><i class="miracle-font-results"></i></div>
                <h3 class="step-card__title">8. Первые продажи и <br>результат</h3>
                <p class="step-card__content">Попадание сайта в топ10 и <br>увеличение клиентов и <br>посетителей на сайте</p>
            </div>
        </div>
    </div>
</div>
