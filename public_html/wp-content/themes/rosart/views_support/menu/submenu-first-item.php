<menuitem  class="top-menu__item" data-dropdown>
    <a href="<% url %>">
        <p class="top-menu__link"><% title %></p>
    </a>
    <ul class="top-menu__drop">
