<div class="choose-us" style="background-image: url(<?= $theme_dir_uri ?>/images/8section.min.jpg)" data-src="<?= $theme_dir_uri ?>/images/8section.png">
    <div class="choose-us__content">
        <h2 class="choose-us__block-title miracle-title miracle-title_primary">Почему именно мы</h2>
        <div class="choose-us__choose-card choose-card">
            <div class="choose-card__block">
                <i class="choose-card__icon miracle-font-money"></i>
                <h4 class="choose-card__title">Приемлимые <br>цены</h4>
                <p class="choose-card__content">Мы создаем сайты по <br>вполне приемлемым <br>ценам, которые отвечают <br>потребностям самого <br>широкого класса клиентов</p>
            </div>
            <div class="choose-card__block">
                <i class="choose-card__icon miracle-font-adaptive"></i>
                <h4 class="choose-card__title">Адаптивность <br>сайтов</h4>
                <p class="choose-card__content">Все наши проекты <br>адаптивны, и их комфортно <br>можно просматривать с <br>любых мобильных <br>устройств</p>
            </div>
            <div class="choose-card__block">
                <i class="choose-card__icon miracle-font-ui-ux"></i>
                <h4 class="choose-card__title">UI/UX<br>&nbsp;</h4>
                <p class="choose-card__content">Руководствуясь принципами <br>поведения пользователей на <br>сайте, мы создаем только <br>уникальный и интуитивно <br>понятный интерфейс</p>
            </div>
            <div class="choose-card__block">
                <i class="choose-card__icon miracle-font-exclusive_design"></i>
                <h4 class="choose-card__title">Эксклюзивный <br>дизайн</h4>
                <p class="choose-card__content">Наши проекты всегда <br>отвечают самым <br>последним тенценциям в <br>сфере web-дизайна и <br>являются эксклюзивными</p>
            </div>
            <div class="choose-card__block">
                <i class="choose-card__icon miracle-font-seo"></i>
                <h4 class="choose-card__title">SCRUM-подход<br>&nbsp;</h4>
                <p class="choose-card__content">Мы предоставляем <br>отчетность по каждому <br>этапу работы, и вы можете <br>видеть процесс разработки <br>на любом уровне</p>
            </div>
            <div class="choose-card__block">
                <i class="choose-card__icon miracle-font-comfort"></i>
                <h4 class="choose-card__title">Удобство <br>работы</h4>
                <p class="choose-card__content">Мы используем только <br>общедоступные CMS, и вы <br>сами сможете легко <br>редактировать <br>информацию на сайте</p>
            </div>
        </div>
    </div>
</div>
