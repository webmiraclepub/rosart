<p class="single-header__link">
    <a href="<% link %>" target="_blank">
        <i class="<% icon %>"></i>
        <span><% title %></span>
    </a>
</p>
