<div class="support" data-src="<?= $theme_dir_uri ?>/images/13section.png">
    <div class="support__content">
        <h3 class="support__block-title miracle-title">Нам доверяют</h3>
        <div class="support__slider miracle-slider">
            <img data-src="<?= $theme_dir_uri ?>/images/partner1.jpg" alt="">
            <img data-src="<?= $theme_dir_uri ?>/images/partner2.jpg" alt="">
            <img data-src="<?= $theme_dir_uri ?>/images/partner3.jpg" alt="">
            <img data-src="<?= $theme_dir_uri ?>/images/partner4.jpg" alt="">
            <img data-src="<?= $theme_dir_uri ?>/images/partner5.jpg" alt="">
            <img data-src="<?= $theme_dir_uri ?>/images/partner6.jpg" alt="">
        </div>
    </div>
</div>
