<div class="body-404" style="background-image: url( <?= $theme_dir_uri ?>/images/404.png )">
    <div class="how-work">
        <div class="how-work__content">
            <div class="body-404__how-work how-work__info">
                <h3 class="body-404__title">Ошибка <span>404</span></h3>
                <h4 class="body-404__subtitle">Данная страница не найдена</h4>
                <p class="body-404__content">Возможно, вы не правильно ввели адрес или она была удалена. Вы можете перейти на <a href="/">главную страницу</a></p>
            </div>
            <img class="body-404__image how-work__image" data-src="<?= $theme_dir_uri ?>/images/404Image.png" alt="">
        </div>
    </div>
</div>
