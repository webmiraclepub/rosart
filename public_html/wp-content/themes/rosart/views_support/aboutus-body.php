<?php $images_uri = get_template_directory_uri() . '/images'; ?>
<div class="aboutus single-body">
	<div class="single-body__content">
		<?php
            wp_reset_postdata();
            the_content();
		?>
		<!-- <h1>Интернет-агентство полного цикла “Rosart”</h1>
		<p>Компания “Rosart” была основана в 2016 году. За это время реализовано свыше 100 дизайн-проектов: разработка макетов для web-дизайна, создание логотипов, фирменного стиля, дизайна печатной продукции, а также выполнены многие проекты по разработке сайтов “под ключ”. В 2017 году мы заняли 49 место в рейтинге веб-студий Екатеринбурга и 83 место в Уральском Федеральном округе.</p>
		<div class="aboutus__icons">
			<div class="aboutus__icon">
				<i class="miracle-font-adaptive"></i>
				<p class="aboutus__subheader">адаптивность сайтов</p>
				<p>Все наши проекты адаптивны, и их комфортно можно просматривать с любых мобильных устройств</p>
			</div>
			<div class="aboutus__icon">
				<i class="miracle-font-exclusive_design"></i>
				<p class="aboutus__subheader">эксклюзивный дизайн</p>
				<p>Наши проекты всегда отвечают самым последним тенценциям в сфере web-дизайна и являются эксклюзивными</p>
			</div>
			<div class="aboutus__icon">
				<i class="miracle-font-comfort"></i>
				<p class="aboutus__subheader">удобство работы</p>
				<p>Мы используем только общедоступные CMS, и вы сами сможете легко редактировать информацию на сайте</p>
			</div>
		</div>
		<img src="<?= $images_uri ?>/about_us_img.png" alt="" class="alignleft">
		<p>Каждый сотрудник компании имеет многолетний опыт в своей специализации, и все вместе мы представляем сплоченную команду профессионалов.</p>
		<p>У нас есть три основных направления работы. Первое - мы предоставляем весь спектр услуг по разработке сайтов: начиная с аналитики рынка и выстраивания единой концепции интернет-рекламы, и заканчивая созданием интерактивных прототипов и разработкой эксклюзивного дизайна.</p>
		<p>В сфере продвижения сайтов мы выступаем за комплексный подход по работе над вашим ресурсом. Каждый наш сайт специально оптимизирован под продвижение в поисковых системах. Мы можем также предложить продвижение в социальных сетях: вконтакте, facebook, instagram, настройку контекстной и таргетированной рекламы.</p>
		<p>Но начинать работу над вашим проектом лучше с создания логотипа и разработки фирменного стиля. После заполнения брифа мы создадим для вас современный логотип и разработаем уникальный стиль компании, который сделает вас заметными на рынке.
		За разработкой дизайна вспомогательных материалов (визитки, флаеры, листовки) также можете обратиться к нам. Сделаем все в соответствии с вашими пожеланиями и учитывая все особенности вашей отрасли. Многие наши работы представлены в портфолио, и вы можете ознакомиться с ними.
		Если у вас остались вопросы, вы можете задать их по телефону или связаться с нами удобным для вас способом.</p> -->
		<div class="aboutus__sertificates">
			<div class="aboutus__sertificate">
				<div class="miracle-slider__slide">
					<img src="<?= $images_uri ?>/sertificate-1.jpg" alt="">
					<i class="miracle-font-zoom miracle-slider__icon-zoom"></i>
				</div>
				<p class="aboutus__sertificate-name">49 место</p>
				<p class="aboutus__sertificate-description">в рейтинге веб студий Екатиренбурга</p>
				<a class="aboutus__sertificates-url" href="http://ratingruneta.ru/web/yekaterinburg">ratingruneta.ru/web/yekaterinburg</a>
			</div>
			<div class="aboutus__sertificate">
				<div class="miracle-slider__slide">
					<img src="<?= $images_uri ?>/sertificate-2.jpg" alt="">
					<i class="miracle-font-zoom miracle-slider__icon-zoom"></i>
				</div>
				<p class="aboutus__sertificate-name">83 место</p>
				<p class="aboutus__sertificate-description">В рейтинге веб-студий УрФО</p>
			</div>
		</div>
	</div>
</div>
