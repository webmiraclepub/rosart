<div class="<% block-class %>">
    <a href="<% link %>">
        <i class="<% icon %>"></i>
        <p><% title %></p>
    </a>
</div>
