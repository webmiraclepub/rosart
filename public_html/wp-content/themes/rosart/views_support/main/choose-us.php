<div class="miracle-wow fadeIn" data-wow-duration="1s" data-wow-delay="0s" style="background-image: url(<?= $bgi_lazy ?>); background-size: cover; background-position: center;">
    <div class="choose-us" data-src="<?= $bgi_full ?>">
        <div class="choose-us__content">
            <h2 class="choose-us__block-title miracle-title miracle-title_primary"><?= $title ?></h2>
            <div class="choose-us__choose-card choose-card">
                <?= $choose_card ?>
            </div>
        </div>
    </div>
</div>
