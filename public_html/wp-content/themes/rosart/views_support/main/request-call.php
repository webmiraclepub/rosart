<div class="miracle-wow fadeIn" data-wow-duration="1s" data-wow-delay="500ms" style="background-image: url(<?= $bgi_lazy ?>); background-size: cover; background-position: center;">
    <div class="send-requery" data-src="<?= $bgi_full ?>">
        <div class="send-requery__block">
            <h3 class=" miracle-title"><?= $title ?></h3>
            <p class="miracle-content miracle-content_white"><?= $content ?></p>
            <button class="send-requery__button miracle-button" data-modal="miracle-modal-send-phone" type="button" name="button">Заказать звонок</button>
        </div>
    </div>
</div>
