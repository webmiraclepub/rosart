<?php

function miracle_get_menuitems( $slug = '', $subitems = true, $theme = '' ){
    $output = '';
    $file = get_template_directory() . '/views_support/menu/menu-item.php';
    $submenu_first_item_file = get_template_directory() . '/views_support/menu/submenu-first-item.php';
    $submenu_item_file = get_template_directory() . '/views_support/menu/submenu-item.php';
    $submenu_last_item_file = get_template_directory() . '/views_support/menu/submenu-last-item.php';
    $mobile_file = get_template_directory() . '/views_support/menu/mobile-menu.php';
    $footer_file = get_template_directory() . '/views_support/menu/footer-menu.php';

    $location = get_nav_menu_locations();
    if( isset( $location[ $slug ] ) ):
        $menuObj = wp_get_nav_menu_object( $location[ $slug ] );
        if( $menuObj ):
            $menu_items = wp_get_nav_menu_items( $menuObj, array() );
        endif;
    endif;

    if( isset( $menu_items ) && !empty( $menu_items ) ):
        for( $i = 0; $i < count( $menu_items ); $i++ ):
            $item = $menu_items[ $i ];
            if( $theme == 'mobile' ):
                $block = file_get_contents( $mobile_file );
            elseif( $theme == 'footer' ):
                $block = file_get_contents( $footer_file );
            elseif( $subitems ):
                $next = ( isset( $menu_items[ $i + 1 ] ) ) ? $menu_items[ $i + 1 ] : false;
                if( $item->menu_item_parent ):
                    if( $next ):
                        $parent = ( $next->menu_item_parent ) ? true : false;
                        if( $parent ):
                            $block = file_get_contents( $submenu_item_file );
                        else:
                            $block = file_get_contents( $submenu_last_item_file );
                        endif;
                    else:
                        $block = file_get_contents( $submenu_last_item_file );
                    endif;
                else:
                    if( $next ):
                        $parent = ( $next->menu_item_parent ) ? true : false;
                        if( $parent ):
                            $block = file_get_contents( $submenu_first_item_file );
                        else:
                            $block = file_get_contents( $file );
                        endif;
                    else:
                        $block = file_get_contents( $file );
                    endif;
                endif;
            else:
                $block = get_file_contents( $file );
            endif;

            $url = $item->url;
            $title = $item->title;

            $block = str_replace( '<% url %>', $url, $block );
            $block = str_replace( '<% title %>', $title, $block );

            $output .= $block;
        endfor;
    endif;

    return $output;
}

function miracle_get_home_header_menu( $block_class = 'header-slider__info-card' ){
    $output = '';

    if( is_page_template( 'pagetemplates/main.php' ) ):
        $file = get_template_directory() . '/views_support/main/header_menu/menu-item.php';

        $menuitems = get_field( 'miracle-main-page-menu' );

        if( $menuitems ):
            foreach( $menuitems as $item ):
                $block = file_get_contents( $file );

                $title = $item['title'];
                $link = $item['link'];
                $icon = ( $item['icoomon'] ) ? $item['icon-icommon'] : 'fa ' . $item['icon-awesome'];

                $block = str_replace( '<% title %>', $title, $block );
                $block = str_replace( '<% link %>', $link, $block );
                $block = str_replace( '<% icon %>', $icon, $block );
                $block = str_replace( '<% block-class %>', $block_class, $block );

                $output .= $block;
            endforeach;
        endif;
    endif;

    return $output;
}

function miracle_get_header_slider(){
    $output = '';

    if( is_page_template( 'pagetemplates/main.php' ) ):
        $file = get_template_directory() . '/views_support/main/header_slider/slide.php';

        $slides = get_field( 'miracle-main-header-slider' );

        if( $slides ):
            foreach( $slides as $slide ):
                $block = file_get_contents( $file );

                $title = $slide['title'];
                $content = $slide['content'];
                $logo = $slide['logo'];
                $image_right = $slide['image-right'];
                $bg = $slide['bg-image'];
                $link = $slide['link'];

                $block = str_replace( '<% title %>', $title, $block );
                $block = str_replace( '<% content %>', $content, $block );
                $block = str_replace( '<% logo-lazy %>', $logo['sizes']['lazy'], $block );
                $block = str_replace( '<% logo-full %>', $logo['url'], $block );
                $block = str_replace( '<% logo-alt %>', $logo['alt'], $block );
                $block = str_replace( '<% logo-title %>', $logo['title'], $block );
                $block = str_replace( '<% image-right-lazy %>', $image_right['sizes']['lazy'], $block );
                $block = str_replace( '<% image-right-full %>', $image_right['url'], $block );
                $block = str_replace( '<% image-right-alt %>', $image_right['alt'], $block );
                $block = str_replace( '<% image-right-title %>', $image_right['title'], $block );
                $block = str_replace( '<% bg-image-lazy %>', $bg['sizes']['lazy'], $block );
                $block = str_replace( '<% bg-image-full %>', $bg['url'], $block );
                $block = str_replace( '<% link %>', $link, $block );

                $output .= $block;
            endforeach;
        endif;
    endif;

    return $output;
}

function miracle_get_about_us_info_grid(){
    $output = '';

    if( is_page_template( 'pagetemplates/main.php' ) ):
        $file = get_template_directory() . '/views_support/main/about-us/info-grid.php';

        $cards = get_field( 'miracle-main-about-us-1-cards' );

        if( $cards ):
            foreach( $cards as $card ):
                $block = file_get_contents( $file );

                $title = '';
                $title_style = '';
                $icon = '';
                $icon_style = '';

                switch( $card['type'] ){
                    case 'number':
                        $title = $card['title-num'];
                        $icon_style = 'display: none';
                        break;
                    case 'icon_icommon':
                        $icon = $card['title-icommon'];
                        $title_style = 'display: none';
                        break;
                    case 'icon_awesome':
                        $icon = 'fa ' . $card['title-awesome'];
                        $title_style = 'display: none';
                        break;
                    default:
                        break;
                }
                $content = $card['content'];

                $block = str_replace( '<% title %>', $title, $block );
                $block = str_replace( '<% title-style %>', $title_style, $block );
                $block = str_replace( '<% icon %>', $icon, $block );
                $block = str_replace( '<% icon-style %>', $icon_style, $block );
                $block = str_replace( '<% content %>', $content, $block );

                $output .= $block;
            endforeach;
        endif;
    endif;

    return $output;
}

function miracle_get_how_work_our_services(){
    $output = '';

    if( is_page_template( 'pagetemplates/main.php' ) ):
        $file = get_template_directory() . '/views_support/main/how-work/our-service.php';

        $cards = get_field( 'miracle-main-full-cycle-work-service' );

        if( $cards ):
            foreach( $cards as $card ):
                $block = file_get_contents( $file );

                $title = $card['title'];
                $content = $card['content'];

                $block = str_replace( '<% title %>', $title, $block );
                $block = str_replace( '<% content %>', $content, $block );

                $output .= $block;
            endforeach;
        endif;
    endif;

    return $output;
}

function miracle_get_about_us_circle_card(){
    $output = '';

    if( is_page_template( 'pagetemplates/main.php' ) ):
        $file = get_template_directory() . '/views_support/main/about-us/circle-card.php';

        $cards = get_field( 'miracle-main-integrated-promotion-card' );

        if( $cards ):
            foreach( $cards as $card ):
                $block = file_get_contents( $file );

                $icon = ( $card['icommon'] ) ? $card['icon-icommon'] : 'fa ' . $card['icon-awesome'];
                $content = $card['content'];

                $block = str_replace( '<% icon %>', $icon, $block );
                $block = str_replace( '<% content %>', $content, $block );

                $output .= $block;
            endforeach;
        endif;
    endif;

    return $output;
}

function miracle_get_choose_cards(){
    $output = '';

    if( is_page_template( 'pagetemplates/main.php' ) ):
        $file = get_template_directory() . '/views_support/main/choose-us/choose-card.php';

        $cards = get_field( 'miracle-main-choose-us-adventage' );

        if( $cards ):
            foreach( $cards as $card ):
                $block = file_get_contents( $file );

                $icon = ( $card['icommon'] ) ? $card['icon-icommon'] : 'fa ' . $card['icon-awesome'];
                $title = $card['title'];
                $content = $card['content'];

                $block = str_replace( '<% icon %>', $icon, $block );
                $block = str_replace( '<% title %>', $title, $block );
                $block = str_replace( '<% content %>', $content, $block );

                $output .= $block;
            endforeach;
        endif;
    endif;

    return $output;
}

function miracle_get_large_price_list_card( $type = '' ){
    $output = '';

    if( is_page_template( 'pagetemplates/main.php' ) ):
        $file = get_template_directory() . '/views_support/main/price-list/';
        $file = ( $type == 'tabs' ) ? $file .'medium-card.php' : $file . 'large-card.php';

        $cards = get_field( 'miracle-main-price-list-card' );

        if( $cards ):
            foreach( $cards as $key => $card ):
                $block = file_get_contents( $file );

                $active = ( $key == 0 ) ? 'true' : 'false';
                $icon = ( $card['icommon'] ) ? $card['icon-icommon'] : 'fa ' . $card['icon-awesome'];
                $title = $card['title'];
                $service_list = miracle_get_price_list_service( $card['list'], $type );
                $price = $card['price'];

                $block = str_replace( '<% active %>', $active, $block );
                $block = str_replace( '<% icon %>', $icon, $block );
                $block = str_replace( '<% title %>', $title, $block );
                $block = str_replace( '<% service-list %>', $service_list, $block );
                $block = str_replace( '<% price %>', $price, $block );

                $output .= $block;
            endforeach;
        endif;
    endif;

    return $output;
}

function miracle_get_price_list_service( $services = array(), $type = '' ){
    $output = '';

    if( is_page_template( 'pagetemplates/main.php' ) ):
        $file = get_template_directory() . '/views_support/main/price-list/';
        $file = ( $type == 'tabs' ) ? $file . 'service-medium.php' : $file . 'service.php';

        if( $services ):
            foreach( $services as $service ):
                $block = file_get_contents( $file );

                $title = $service['in'];

                $block = str_replace( '<% title %>', $title, $block );

                $output .= $block;
            endforeach;
        endif;
    endif;

    return $output;
}

function miracle_get_work_step_cards(){
    $output = '';

    if( is_page_template( 'pagetemplates/main.php' ) ):
        $file = get_template_directory() . '/views_support/main/work-step/work-card.php';

        $cards = get_field( 'miracle-main-work-step-card' );

        if( $cards ):
            $step = 1;
            foreach( $cards as $card ):
                $block = file_get_contents( $file );

                $color = $card['color'];
                $icon = ( $card['icommon'] ) ? $card['icon-icommon'] : 'fa ' . $card['icon-awesome'];
                $title = $step . '. ' . $card['title'];
                $content = $card['content'];

                $block = str_replace( '<% color %>', $color, $block );
                $block = str_replace( '<% icon %>', $icon, $block );
                $block = str_replace( '<% title %>', $title, $block );
                $block = str_replace( '<% content %>', $content, $block );

                $output .= $block;
                $step++;
            endforeach;
        endif;
    endif;

    return $output;
}

function miracle_get_slide_images( $gallery = array() ){
    $output = '';
    $file = get_template_directory() . '/views_support/global/slider/image.php';

    if( empty( $gallery ) )
        return $output;

    foreach( $gallery as $image ):
        $block = file_get_contents( $file );

        $full = $image['url'];
        $lazy = $image['sizes']['lazy'];
        $alt = $image['alt'];
        $title = $image['title'];

        $block = str_replace( '<% full %>', $full, $block );
        $block = str_replace( '<% lazy %>', $lazy, $block );
        $block = str_replace( '<% title %>', $title, $block );
        $block = str_replace( '<% alt %>', $alt, $block );

        $output .= $block;
    endforeach;

    return $output;
}

function miracle_get_main_last_portfolio( $posts = array() ){
    $output = '';
    $file = get_template_directory() . '/views_support/main/last-portfolio/post-card.php';

    if( empty( $posts ) )
        return $output;

    foreach( $posts as $post ):
        $block = file_get_contents( $file );

        $bgi = get_the_post_thumbnail_url( $post, 'medium' );
        $title = get_the_title( $post );
        $link = get_post_permalink( $post->ID );

        $block = str_replace( '<% bgi %>', $bgi, $block );
        $block = str_replace( '<% title %>', $title, $block );
        $block = str_replace( '<% link %>', $link, $block );

        $output .= $block;
    endforeach;

    return $output;
}

function miracle_get_global_form( $modal = '' ){
    $output = '';
    $file = get_template_directory() . '/views_support/global/form/';

    switch ( $modal ) {
        case 'send-list':
            $file .= 'send-list.php';
            $slug = 'miracle-global-modal-3';
            break;
        case 'audit':
            $file .= 'audit.php';
            $slug = 'miracle-global-modal-2';
            break;
        case 'send-phone':
            $file .= 'send-phone.php';
            $slug = 'miracle-global-modal-1';
            break;
        default:
            return $output;
            break;
    }

    $block = file_get_contents( $file );

    $bgi = get_field( $slug . '-bg', 'option' );
    $bgi_lazy = $bgi['sizes']['lazy'];
    $bgi_full = $bgi['url'];
    $title = get_field( $slug . '-title', 'option' );
    $subtitle = get_field( $slug . '-subtitle', 'option' );

    $block = str_replace( '<% bgi-lazy %>', $bgi_lazy, $block );
    $block = str_replace( '<% bgi-full %>', $bgi_full, $block );
    $block = str_replace( '<% title %>', $title, $block );
    $block = str_replace( '<% subtitle %>', $subtitle, $block );

    $output .= $block;

    return $output;
}

function miracle_get_services_list(){
    $output = '';
    $file_odd = get_template_directory() . '/views_support/services/cards/odd-card.php';
    $file_even = get_template_directory() . '/views_support/services/cards/even-card.php';
    if( !is_page_template( 'pagetemplates/main.php' ) )
        $output;

    $services = get_field( 'miracle-service-cards' );
    if( empty( $services ) )
        return $output;

        foreach( $services as $key => $service ):
            $block = ( $key%2 == 0 ) ? file_get_contents( $file_odd ) : file_get_contents( $file_even );

            $image = $service['image'];
            $image_src = $image['sizes']['medium_large'];
            $image_alt = $image['alt'];
            $image_title = $image['title'];
            $title = $service['title'];
            $content = $service['content'];
            $icon = ( $service['icommon'] ) ? $service['icon-icommon'] : 'fa ' . $service['icon-awesome'];
            $link = $service['link'];

            $block = str_replace( '<% image-src %>', $image_src, $block );
            $block = str_replace( '<% image-alt %>', $image_alt, $block );
            $block = str_replace( '<% image-title %>', $image_title, $block );
            $block = str_replace( '<% title %>', $title, $block );
            $block = str_replace( '<% content %>', $content, $block );
            $block = str_replace( '<% icon %>', $icon, $block );
            $block = str_replace( '<% link %>', $link, $block );

            $output .= $block;
        endforeach;

        return $output;
}

function miracle_get_breadcrumbs( $breadcrumbs = array() ){
    $output = '';
    $file = get_template_directory() . '/views_support/global/breadcrumbs/item.php';

    if( is_page_template( 'pagetemplates/main.php' ) || empty( $breadcrumbs ) )
        $output;

    foreach( $breadcrumbs as $title => $link ):
        $block = file_get_contents( $file );
        $block = str_replace( '<% title %>', $title, $block );
        $block = str_replace( '<% link %>', $link, $block );
        $output .= $block;
    endforeach;

    return $output;
}

function miracle_get_portfolio_filter( $page = '' ){
    $output = '';
    $file = get_template_directory() . '/views_support/portfolio/archive-filter/filter.php';

    $terms = get_terms(
    	array(
    		'taxonomy' => 'design',
    		'hide_empty' => true,
            'orderby' => 'id'
    	)
    );

    if( empty( $terms ) )
        $output;

    foreach( $terms as $term ):
        $block = file_get_contents( $file );
        $title = $term->name;
        $class = ( $term->slug == get_query_var( 'term' ) ) ? 'archive-filter__filters_active' : '';
        $url = get_term_link( $term->term_id, 'design' );
        $block = str_replace( '<% title %>', $title, $block );
        $block = str_replace( '<% class %>', $class, $block );
        $block = str_replace( '<% url %>', $url, $block );
        $output .= $block;
    endforeach;

    return $output;
}

function miracle_get_portfolio_post_cards(){
    $output = '';
    $file = get_template_directory() . '/views_support/portfolio/post/card.php';

    while( have_posts() ):
        the_post();
        $block = file_get_contents( $file );
        $post = get_post();

        $title = $post->post_title;
        $date = get_the_date( 'd.m.Y' );
        $time = get_the_date( 'H:i' );
        $link = get_post_permalink();
        $image_full = get_the_post_thumbnail_url( $post, 'full' );
        $image_lazy = get_the_post_thumbnail_url( $post, 'lazy' );

        $block = str_replace( '<% title %>', $title, $block );
        $block = str_replace( '<% date %>', $date, $block );
        $block = str_replace( '<% time %>', $time, $block );
        $block = str_replace( '<% link %>', $link, $block );
        $block = str_replace( '<% image-full %>', $image_full, $block );
        $block = str_replace( '<% image-lazy %>', $image_lazy, $block );

        $output .= $block;
    endwhile;
    wp_reset_postdata();

    return $output;
}

function miracle_get_pagenation(){
    $args = array(
        // 'total'  => 76,
        // 'current' => 45,
        'end_size'     => 1,
        'mid_size'     => 1,
        'prev_next'    => True,
        'prev_text'    => '<span>Предыдущая</span>',
        'next_text'    => '<span>Следующая</span>',
        'type'         => 'plain',
        'add_args'     => False,
        'add_fragment' => '',
        'before_page_number' => '<span>',
        'after_page_number'  => '</span>'
    );

    $nav = paginate_links( $args );

    $nav = str_replace( 'page-numbers current', 'pagenation__item pagenation__active', $nav );
    $nav = str_replace( 'next page-numbers', 'pagenation__item pagenation__next', $nav );
    $nav = str_replace( 'prev page-numbers', 'pagenation__item pagenation__prev', $nav );
    $nav = str_replace( 'page-numbers', 'pagenation__item pagenation__page', $nav );
    $nav = str_replace( 'pagenation__item pagenation__page dots', 'pagenation__item pagenation__center', $nav );
    $nav = str_replace( '&hellip;', '<span>&hellip;</span>', $nav );

    return $nav;
}

function miracle_get_portfolio_work_link(){
    $output = '';
    $file = get_template_directory() . '/views_support/single-portfolio/header/work-link.php';

    if( ! is_singular( 'portfolio' ) || ! get_field( 'miracle-portfolio-post-is-work-link' ) )
        return $output;

    $outer = file_get_contents( $file );
    $title = get_field( 'miracle-portfolio-post-link-title' );
    $link = get_field( 'miracle-portfolio-post-link-url' );
    $icon = ( get_field( 'miracle-portfolio-post-icommon' ) ) ? get_field( 'miracle-portfolio-post-link-icon-icommon' ) : 'fa ' . get_field( 'miracle-portfolio-post-link-icon-awesome' );

    $outer = str_replace( '<% icon %>', $icon, $outer );
    $outer = str_replace( '<% title %>', $title, $outer );
    $outer = str_replace( '<% link %>', $link, $outer );

    return $outer;
}

function miracle_get_blog_post_cards(){
    $output = '';
    $file = get_template_directory() . '/views_support/home/post/card.php';

    while( have_posts() ):
        the_post();
        $block = file_get_contents( $file );
        $post = get_post();

        $title = $post->post_title;
        $content = get_the_excerpt();
        $date = get_the_date( 'd.m.Y' );
        $time = get_the_date( 'H:i' );
        $link = get_post_permalink();
        $image_full = get_the_post_thumbnail_url( $post, 'full' );
        $image_lazy = get_the_post_thumbnail_url( $post, 'lazy' );

        $block = str_replace( '<% title %>', $title, $block );
        $block = str_replace( '<% content %>', $content, $block );
        $block = str_replace( '<% date %>', $date, $block );
        $block = str_replace( '<% time %>', $time, $block );
        $block = str_replace( '<% link %>', $link, $block );
        $block = str_replace( '<% image-full %>', $image_full, $block );
        $block = str_replace( '<% image-lazy %>', $image_lazy, $block );

        $output .= $block;
    endwhile;
    wp_reset_postdata();

    return $output;
}

function miracle_get_about_us_services(){
    $output = '<div class="aboutus__icons">';
    $file = get_template_directory() . '/views_support/about-us/service-card/card.php';

    if( ! is_page_template( 'pagetemplates/aboutus.php' ) )
        $output;

    $services = get_field( 'miracle-about-us-service' );

    if( empty( $services ) )
        return $output;

    foreach( $services as $service ):
        $block = file_get_contents( $file );

        $title = $service['title'];
        $content = $service['content'];
        $icon = ( $service['icommon'] ) ? $service['icon-icommon'] : 'fa ' . $service['icon-awesome'];

        $block = str_replace( '<% title %>', $title, $block );
        $block = str_replace( '<% content %>', $content, $block );
        $block = str_replace( '<% icon %>', $icon, $block );
        $output .= $block;
    endforeach;

    return $output . '</div>';
}

function miracle_get_about_us_sertificate(){
    $output = '';
    $file = get_template_directory() . '/views_support/about-us/sertificate/card.php';

    if( ! is_page_template( 'pagetemplates/aboutus.php' ) )
        $output;

    $setificate = get_field( 'miracle-about-us-sertificate' );

    if( empty( $setificate ) )
        return $output;

    foreach( $setificate as $card ):
        $block = file_get_contents( $file );

        $image = $card['image'];
        $image_lazy = $image['sizes']['lazy'];
        $image_full = $image['url'];
        $image_alt = $image['alt'];
        $image_title = $image['title'];
        $title = $card['title'];
        $content = $card['content'];
        $link = ( $card['link'] ) ? '<a class="aboutus__sertificates-url" href="' . $card['url'] . '">' . $card['link-text'] . '</a>' : '';

        $block = str_replace( '<% title %>', $title, $block );
        $block = str_replace( '<% content %>', $content, $block );
        $block = str_replace( '<% image-lazy %>', $image_lazy, $block );
        $block = str_replace( '<% image-full %>', $image_full, $block );
        $block = str_replace( '<% image-alt %>', $image_alt, $block );
        $block = str_replace( '<% image-title %>', $image_title, $block );
        $block = str_replace( '<% link %>', $link, $block );
        $output .= $block;
    endforeach;

    return $output . '';
}
