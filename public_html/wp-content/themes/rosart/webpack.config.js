const path = require('path');
var CleanWebpackPlugin = require('clean-webpack-plugin');
var BrowserSyncPlugin = require('browser-sync-webpack-plugin');
var webpack = require('webpack');
var autoprefixer = require('autoprefixer');
var UglifyJSPlugin = require('uglifyjs-webpack-plugin');

var node_modules = path.resolve(__dirname, 'node_modules');
var pathToBourbon = require('bourbon').includePaths;
var pathToSassSetings = path.resolve(__dirname, "src") + '/globals';

const config = {
	context: __dirname,
	devtool: "source-map",
	entry: {
		index: "./src/pages/index.js",
		archivePost: "./src/pages/archive-post.js",
		archivePortfolio: "./src/pages/archive-portfolio.js",
		singlePortfolio: "./src/pages/single-portfolio.js",
		single: "./src/pages/single.js",
		services: './src/pages/services.js',
		errorPage: './src/pages/404.js',
		contactPage: './src/pages/contact-page.js',
		aboutusPage: './src/pages/aboutus-page.js',
		thanksPage: './src/pages/thanks-page.js'
	},
	output: {
		path: path.resolve(__dirname, "dist"),
		filename: "[name].miracle.js"
	},
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /(node_modules|bower_components)/,
				use: [{
					loader: 'babel-loader',
					options: {
						presets: ['env']
					}
				}]
			},{
				test: /\.css$/,
				use: [
					{
						loader: 'style-loader'
					},{
						loader: 'css-loader',
						options: {
							modules: true
						}
					}
				],
				exclude: node_modules
			},{
				test: /\.scss$/,
				use: [{
						loader: "style-loader"
					},{
						loader: "css-loader"
					},{
						loader: 'postcss-loader',
					    options: {
					        plugins: [
					            autoprefixer({
					                browsers:['ie >= 8', 'last 4 version']
					            })
					        ],
					        sourceMap: true
					    }
					},{
						loader: "sass-loader",
						options: {
							includePaths: [pathToBourbon, pathToSassSetings],
							sourceMap: true
						}
					}
				],
				exclude: node_modules
			},{
				test: /\.(png|jpg|gif)$/,
				use: [{
					loader: 'file-loader',
					options: {
					    name: '[path][name].[ext]',
					    // publicPath: '/dist/'
						publicPath: '/wp-content/themes/rosart/dist/'
					}
				}]
			},{
				test: /\.(woff|woff2|eot|ttf|otf|svg)$/,
				use: [{
					loader: 'file-loader',
					options: {
					    name: '[path][name].[ext]',
					    // publicPath: '/dist/'
						publicPath: '/wp-content/themes/rosart/dist/'
					}
				}]
			}
		]
	},
	plugins: [
		new CleanWebpackPlugin(['dist', 'build'], {
			root: __dirname,
			verbose: true,
			dry: false,
			exclude: ['./src/pages/*/*.js']
		}),
		new UglifyJSPlugin({
			sourceMap: true
		}),
		new BrowserSyncPlugin({
			//path for dev sites
			// host: 'rosart',
			port: 3000,
			// server: { baseDir: ['.'], directory: true, index: 'index.html' }
			proxy: 'http://rosart/'
		})
	]
};



module.exports = config;
