import { MiracleModal } from '../content-block/class/MiracleModal.class.js';
import LazyLoad from 'vanilla-lazyload/src/lazyload.js';
require('./style/component.scss');

try{
    let info = document.querySelectorAll('.info-grid');

    for (var i = 0; i < info.length; i++) {

        let content = info[i].querySelectorAll('.info-grid__block-content');
        let max_content = 0;
        for (var x = 0; x < content.length; x++) {
            max_content = ( max_content > content[x].offsetHeight ) ? max_content : content[x].offsetHeight;
        }

        let block = info[i].querySelectorAll('.info-grid__block');
        for (let j = 0; j < block.length; j++) {
            block[j].querySelector('.info-grid__block-content').style.height = max_content + 'px';
        }
    }

    let circle = document.querySelectorAll('.circle-card');

    for (var i = 0; i < circle.length; i++) {

        let content = circle[i].querySelectorAll('.circle-card__block-content');
        let max_content = 0;
        for (var x = 0; x < content.length; x++) {
            max_content = ( max_content > content[x].offsetHeight ) ? max_content : content[x].offsetHeight;
        }

        let block = circle[i].querySelectorAll('.circle-card__block');
        for (let j = 0; j < block.length; j++) {
            block[j].querySelector('.circle-card__block-content').style.height = max_content + 'px';
        }
    }

    let bgLazy = new LazyLoad({
        elements_selector: '.about-us',
        threshold: 50
    });

    let imgLazy = new LazyLoad({
        elements_selector: 'img.about-us__image',
        threshold: 50
    });

    let modal = document.querySelectorAll('.about-us__button[data-modal]');

    for (let i = 0; i < modal.length; i++) {
        let obj = new MiracleModal( modal[i] );
    }
}catch(e){
    console.log(e);
}
