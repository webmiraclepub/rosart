import LazyLoad from 'vanilla-lazyload/src/lazyload.js';
require('./style/component.scss');

try {
    document.addEventListener('DOMContentLoaded', function(){

        let w = document.body.offsetWidth;
        if( w < 1200 ){

            let getTemplate = new Promise((resolve, reject) => {
                let root = document.querySelector('.last-portfolio');
                let template = document.querySelector('template[type="last-portfolio-medium"]').content.cloneNode(true);
                root.appendChild(template);

                resolve();
            });

            getTemplate.then(
                result => {
                    let bg_lazy = new LazyLoad({
                        elements_selector: '.post-develop-card'
                    });
                }, error => {
                    console.info(error);
            });
        }
    });
} catch (e) {
    console.info(e);
}
