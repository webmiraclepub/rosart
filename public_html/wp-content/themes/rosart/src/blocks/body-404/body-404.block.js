import LazyLoad from 'vanilla-lazyload/src/lazyload.js';
require('./style/component.scss');

try{
    let bgLazy = new LazyLoad({
        elements_selector: '.body-404'
    });

    document.addEventListener( 'DOMContentLoaded', ()=>{
        let h = document.querySelector('.header').offsetHeight + 5;
        let b = document.querySelector('.footer').offsetHeight;

        let node = document.querySelector('.body-404');

        node.style.marginTop = '-' + h + 'px';
        node.style.paddingTop = h + 'px';

        node.style.marginBottom = '-' + b + 'px';
        node.style.paddingBottom = b + 'px';
    });
}
catch(e){
	console.error(e);
}
