import LazyLoad from 'vanilla-lazyload/src/lazyload.js';
require('./style/component.scss');

let choose = document.querySelectorAll('.choose-card');

for (var i = 0; i < choose.length; i++) {

    let title = choose[i].querySelectorAll('.choose-card__title');
    let max_title = 0;
    for (var n = 0; n < title.length; n++) {
        max_title = ( max_title > title[n].offsetHeight ) ? max_title : title[n].offsetHeight;
    }

    let content = choose[i].querySelectorAll('.choose-card__content');
    let max_content = 0;
    for (var x = 0; x < content.length; x++) {
        max_content = ( max_content > content[x].offsetHeight ) ? max_content : content[x].offsetHeight;
    }

    let block = choose[i].querySelectorAll('.choose-card__block');
    for (let j = 0; j < block.length; j++) {
        block[j].querySelector('.choose-card__title').style.height = max_title + 'px';
        block[j].querySelector('.choose-card__content').style.height = max_content + 'px';
    }
}

let bgLazy = new LazyLoad({
    elements_selector: '.choose-us',
    threshold: 50
});
