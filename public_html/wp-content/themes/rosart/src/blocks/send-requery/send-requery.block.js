import { MiracleModal } from '../content-block/class/MiracleModal.class.js';
import LazyLoad from 'vanilla-lazyload/src/lazyload.js';
require('./style/component.scss');

try {
    let bgLazy = new LazyLoad({
        elements_selector: '.send-requery',
        threshold: 50
    });

    let modal = document.querySelectorAll('.send-requery__button[data-modal]');

    for (let i = 0; i < modal.length; i++) {
        let obj = new MiracleModal( modal[i] );
    }
} catch (e) {
    console.log(e);
}
