import { MiracleModal } from '../content-block/class/MiracleModal.class.js';
import LazyLoad from 'vanilla-lazyload/src/lazyload.js';

require('./style/component.scss');

try {
    let modal = document.querySelectorAll('.footer__modal-button[data-modal]');

    for (let i = 0; i < modal.length; i++) {
        let obj = new MiracleModal( modal[i] );
    }

    let bgLazy = new LazyLoad({
        elements_selector: '.footer',
        threshold: 50
    });

} catch (e) {
    console.info(e);
}
