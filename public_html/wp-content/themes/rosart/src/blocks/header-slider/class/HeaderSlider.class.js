class HeaderSlider{
	constructor(item, slider){
		this.header = item;
		this.slider = slider;

		this.slideShow = true;

		this.initSlider();
		this.setmargin();

		this.listenNav();
		this.autoSlide();
	}

	initSlider(){
		let slides = this.slider.querySelectorAll('.header-slider__slide');
		this.slides_count = slides.length;
		this.position = 1;

		let styles = '';
		for (let i = 0; i < this.slides_count; i++) {
			slides[i].style.width = document.querySelector('body').offsetWidth + 'px';
			let startLeft = 100 - i * 100;
			let startRight = -100 - i * 100;
			let end = 0 - i * 100;
			styles += `
@keyframes headerSlide_${i+1}_ShowLeft{
	0%{
		transform: translateX(${startLeft}%);
		opacity: 0;
	}50%{
		opacity: 0;
	}100%{
		transform: translateX(${end}%);
		opacity: 1;
	}
}
.headerSlide_${i+1}_ShowLeft{
	animation: headerSlide_${i+1}_ShowLeft .6s ease forwards;
}
@keyframes headerSlide_${i+1}_ShowRight{
	0%{
		transform: translateX(${startRight}%);
		opacity: 0;
	}50%{
		opacity: 0;
	}100%{
		transform: translateX(${end}%);
		opacity: 1;
	}
}
.headerSlide_${i+1}_ShowRight{
	animation: headerSlide_${i+1}_ShowRight .6s ease forwards;
}`;
		}

		let styleObj = document.createElement('style');
		styleObj.innerHTML = styles;
		document.body.appendChild( styleObj );
	}

	setmargin(slider){
		let height = this.header.offsetHeight;
		height = height + 10;
		this.slider.style.top = '-' + height + 'px';
		this.slider.style.marginBottom = '-' + height + 'px';
	}

	autoSlide(){
		let self = this;

		setInterval(() =>{
				if( self.slideShow ){
					let pos = ( self.position != self.slides_count ) ? ( self.position + 1 ) : 1;
					self.moveSlide( pos, true );
				}
			}, 7000);
	}

	listenNav(){
		let self = this;
		this.slider.querySelector('.header-slider__prev').addEventListener('click', function(){
			self.prevSlide();
		});
		this.slider.querySelector('.header-slider__next').addEventListener('click', function(){
			self.nextSlide();
		});
	}

	nextSlide(){
		let slide = 1;
		if (this.position < this.slides_count){
			slide = this.position + 1;
		}else{
			slide = 1;
		}
		this.moveSlide( slide, true );
	}

	prevSlide(){
		let slide = this.slides_count;
		if (this.position > 1){
			slide = this.position - 1;
		}else{
			slide = this.slides_count;
		}
		this.moveSlide( slide, false );
	}

	moveSlide( slide, left ){
		this.slider.querySelector(`.header-slider__slide:nth-of-type(${slide})`).style.zIndex = 3;

		let slideObjStart = this.slider.querySelectorAll(`.header-slider__slide`);
		for (var i = 0; i < slideObjStart.length; i++) {
			slideObjStart[i].querySelector('.header-slider__info').classList.remove('header-slider__info_show');
		}
		if( left ){
			let c = `headerSlide_${slide}_ShowLeft`;
			if( this.position != this.slides_count ){
				let rc = `headerSlide_${slide-1}_ShowRight`;
				let lc = `headerSlide_${slide-1}_ShowLeft`;
				setTimeout( ()=>{
					this.slider.querySelector(`.header-slider__slide:nth-of-type(${slide-1})`).classList.remove(rc);
					this.slider.querySelector(`.header-slider__slide:nth-of-type(${slide-1})`).classList.remove(lc);
				}, 600);
			}else{
				const rc = `headerSlide_${this.slides_count}_ShowRight`;
				const lc = `headerSlide_${this.slides_count}_ShowLeft`;
				setTimeout( ()=>{
					this.slider.querySelector(`.header-slider__slide:nth-of-type(${this.slides_count})`).classList.remove(rc);
					this.slider.querySelector(`.header-slider__slide:nth-of-type(${this.slides_count})`).classList.remove(lc);
				}, 600);
			}
			this.slider.querySelector(`.header-slider__slide:nth-of-type(${slide})`).classList.add(c);
		}else{
			let c = `headerSlide_${slide}_ShowRight`;

			if( this.position != 1 ){
				let rc = `headerSlide_${slide+1}_ShowRight`;
				let lc = `headerSlide_${slide+1}_ShowLeft`;
				setTimeout( ()=>{
					this.slider.querySelector(`.header-slider__slide:nth-of-type(${slide+1})`).classList.remove(rc);
					this.slider.querySelector(`.header-slider__slide:nth-of-type(${slide+1})`).classList.remove(lc);
				}, 600);
			}else{
				const rc = `headerSlide_1_ShowRight`;
				const lc = `headerSlide_1_ShowLeft`;
				setTimeout( ()=>{
					this.slider.querySelector(`.header-slider__slide:nth-of-type(1)`).classList.remove(rc);
					this.slider.querySelector(`.header-slider__slide:nth-of-type(1)`).classList.remove(lc);
				}, 600);
			}
			this.slider.querySelector(`.header-slider__slide:nth-of-type(${slide})`).classList.add(c);
		}
		setTimeout( ()=>{
			let slideObj = this.slider.querySelectorAll(`.header-slider__slide`);
			for (var i = 0; i < slideObj.length; i++) {
				slideObj[i].style.zIndex = 1;
			}
			let active = this.slider.querySelector(`.header-slider__slide:nth-of-type(${slide})`);
			active.style.zIndex = 2;
			active.querySelector('.header-slider__info').classList.add('header-slider__info_show');
		}, 600);
		this.position = slide;
		this.slideShow = false;
		let self = this;
		setTimeout( ()=>{
			self.slideShow = true;
		}, 7000);
	}
}

export {HeaderSlider};
