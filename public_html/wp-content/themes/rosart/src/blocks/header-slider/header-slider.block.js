import {HeaderSlider} from './class/HeaderSlider.class';
import {MiracleModal} from '../content-block/class/MiracleModal.class.js';
import LazyLoad from 'vanilla-lazyload/src/lazyload.js';

require('./style/app.scss');

let nav_position = function(){
    let w = document.body.offsetWidth;

    if( w > 1408 ){
        let slider = document.querySelector('.header-slider');
        let leftNav = slider.querySelector('.header-slider__prev');
        let rightNav = slider.querySelector('.header-slider__next');
        let margin = ( w - 1408 ) / 4;
        if( margin > 0 ){
            leftNav.style.left = margin + 'px';
            rightNav.style.right = margin + 'px';
        }
    }
}

document.addEventListener('DOMContentLoaded', function(){

    let w = document.body.offsetWidth;
    if( w > 1350 ){

        let getTemplate = new Promise((resolve, reject) => {
            let root = document.querySelector('.header-slider');
            let template = document.querySelector('template[type="header-large-slider"]').content.cloneNode(true);
            root.appendChild(template);

            resolve();
        });

        getTemplate.then(
            result => {
                let searchElem = document.querySelector('.header');
                let slider = document.querySelector('.header-slider');
                let searchObj  = new HeaderSlider( searchElem, slider );

                let bg_lazy = new LazyLoad({
                    container: slider.querySelector('.header-slider__outer'),
                    elements_selector: '.header-slider__slide-lazy-bg'
                });

                let img_lazy = new LazyLoad({
                    container: slider.querySelector('.header-slider__outer'),
                    elements_selector: 'img[data-src]'
                });

                // let slider = document.querySelector('.header-slider');
                let leftNav = slider.querySelector('.header-slider__prev');
                let rightNav = slider.querySelector('.header-slider__next');
                let margin = ( w - 1408 ) / 4;
                if( margin > 0 ){
                    leftNav.style.left = margin + 'px';
                    rightNav.style.right = margin + 'px';
                }

                window.addEventListener('resize', nav_position );

                // calc offset for services blocks

                setTimeout(function(){
                    let cards = document.querySelector('#header-slider__info-cards');
                    cards.style.marginBottom = '-' + cards.offsetHeight + 'px';
                    cards.style.transform = 'translateY(calc(-100% - 30px))';
                } , 1000);

                // calc offset for services blocks

            }, error => {
                console.info(error);
        });
    }else{

        let getTemplate = new Promise((resolve, reject) => {
            let root = document.querySelector('.header-slider');
            let template = document.querySelector('template[type="header-medium-slider"]').content.cloneNode(true);
            root.appendChild(template);

            resolve();
        });

        getTemplate.then(
            result => {
                let headerSlider = document.querySelector('.header-slider');
                let margin = '-' + ( headerSlider.previousElementSibling.offsetHeight + 4 ) + 'px';
                let padding = ( w > 750 ) ? ( headerSlider.previousElementSibling.offsetHeight + 100 ) : ( headerSlider.previousElementSibling.offsetHeight + 50 );

                headerSlider.style.top = margin;
                headerSlider.style.marginBottom = margin;
                headerSlider.querySelector('.header-medium').style.paddingTop = padding + 'px';

                let modal = document.querySelectorAll('.header-medium__button[data-modal]');

                for (let i = 0; i < modal.length; i++) {
                    let obj = new MiracleModal( modal[i] );
                }

                let bg_lazy = new LazyLoad({
                    elements_selector: '.header-medium'
                });

                let logo_lazy = new LazyLoad({
                    elements_selector: '.header-medium__logo'
                });
            }, error => {
                console.info(error);
        });
    }
});
