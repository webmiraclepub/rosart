// import "inputmask/dist/inputmask/inputmask.numeric.extensions";
import Inputmask from "inputmask/dist/inputmask/inputmask.extensions";
require('./style/component.scss');

try {
    let sendForm = document.querySelectorAll('.send-form');
    for (let i = 0; i < sendForm.length; i++) {
        let input = sendForm[i].querySelectorAll('.send-form__input-block');

        for (let j = 0; j < input.length; j++) {
            input[j].querySelector('input').addEventListener( 'focus', (e)=>{
                input[j].querySelector('.send-form__label').classList.add('send-form__label_focus');
            });
            input[j].querySelector('input').addEventListener( 'blur', (e)=>{
                if( ! input[j].querySelector('input').value ){
                    input[j].querySelector('.send-form__label').classList.remove('send-form__label_focus');
                }
            });
        }
    }

    let email = document.querySelectorAll("input[type='email']");

    for (let i = 0; i < email.length; i++) {
        email[i].addEventListener("input", function (event) {
          if (email[i].validity.typeMismatch) {
            email[i].setCustomValidity("Не верный адрес электронной почты");
          } else {
            email[i].setCustomValidity("");
          }
        });
    }

    let number = document.querySelectorAll("input[type='tel']");

    for (let i = 0; i < number.length; i++) {
        let im = new Inputmask("+7(999) 999-99-99");
        im.mask( number[i] );
    }
} catch (e) {
    console.info(e);
}
