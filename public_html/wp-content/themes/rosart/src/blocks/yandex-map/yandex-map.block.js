require('./style/component.scss');

try{
    document.addEventListener( 'DOMContentLoaded', ()=>{
        let map = document.querySelectorAll('#yandex-map');
        if( map.length > 0 ){
            map[0].style.maxWidth = document.body.offsetWidth + 'px';
        }
    });
    window.addEventListener( 'load', function(){

        let node = document.querySelectorAll('#yandex-map[data-page="contacts"]');

        if( node.length > 0 ){
            let h = node[0].nextElementSibling.nextElementSibling.offsetHeight;
            node[0].style.height = h + 'px';
            node[0].style.marginBottom = '-' + h + 'px';
        }


        let script = document.createElement('script');
        script.setAttribute( 'src', 'https://api-maps.yandex.ru/2.1/?lang=ru_RU' );
        document.body.appendChild( script );
        let y_point = miracleYandex.y;
        let x_point = miracleYandex.x;
        let yc_point = miracleYandex.yc;
        let xc_point = miracleYandex.xc;
        let hint = miracleYandex.hint.toString();
        let ballon = miracleYandex.ballon;
        let icon = miracleYandex.icon;

        let script2 = document.createElement('script');
        script2.innerHTML = `ymaps.ready(function () {
                var myMap = new ymaps.Map(\'yandex-map\', {
                        center: [${yc_point}, ${xc_point}],
                        zoom: 14
                    }, {
                        searchControlProvider: \'yandex#search\'
                    }),

                    MyIconContentLayout = ymaps.templateLayoutFactory.createClass(
                        \'<div style="color: #FFFFFF; font-weight: bold;">$[properties.iconContent]</div>\'
                    ),

                    myPlacemark = new ymaps.Placemark([${y_point}, ${x_point}], {
                        hintContent: \'${hint}\',
                        balloonContent: \'${ballon}\'
                    }, {
                        iconLayout: 'default#image',
                        iconImageHref: \'${icon}\',
                        iconImageSize: [50, 80],
                        iconImageOffset: [-25, -40]
                   });
                   myMap.geoObjects.add(myPlacemark);
            });`;
        script.addEventListener( 'load', ()=>{
            document.body.appendChild( script2 );
        });
    });
}catch(e){
    console.info('Yandex map is fail loaded');
    console.info(e);
}
