import LazyLoad from 'vanilla-lazyload/src/lazyload.js';
require('./style/component.scss');

let step = document.querySelectorAll('.step-card');

for (var i = 0; i < step.length; i++) {

    let title = step[i].querySelectorAll('.step-card__title');
    let max_title = 0;
    for (var n = 0; n < title.length; n++) {
        max_title = ( max_title > title[n].offsetHeight ) ? max_title : title[n].offsetHeight;
    }

    let content = step[i].querySelectorAll('.step-card__content');
    let max_content = 0;
    for (var x = 0; x < content.length; x++) {
        max_content = ( max_content > content[x].offsetHeight ) ? max_content : content[x].offsetHeight;
    }

    let block = step[i].querySelectorAll('.step-card__block');
    for (let j = 0; j < block.length; j++) {
        block[j].querySelector('.step-card__title').style.height = max_title + 'px';
        block[j].querySelector('.step-card__content').style.height = max_content + 'px';
    }
}

let bgLazy = new LazyLoad({
    elements_selector: '.work-step',
    threshold: 50
});
