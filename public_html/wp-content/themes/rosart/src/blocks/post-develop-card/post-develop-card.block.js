require('./style/component.scss');
import LazyLoad from 'vanilla-lazyload/src/lazyload.js';


try {
    let bgLazy = new LazyLoad({
        elements_selector: '.post-develop-card',
        threshold: 50
    });
} catch (e) {
    console.info( e );
}
