import {MiracleSlider} from './class/MiracleSlider.class.js';
import LazyLoad from 'vanilla-lazyload/src/lazyload.js';


require('./style/component.scss');

try {
    document.addEventListener( 'DOMContentLoaded', ()=>{
        let slides = document.querySelectorAll('.miracle-slider.sertificate__slider');
        for (var i = 0; i < slides.length; i++) {
            let options = {
                images: 2,
                modal: true
            }
            let slideObj = new MiracleSlider( slides[i], options );

            let lazy = new LazyLoad({
                container: slides[i].querySelector('.miracle-slider__content'),
                elements_selector: '.miracle-slider__slide:nth-of-type(n+4) img'
            });
            let lazyActiveImage = new LazyLoad({
                threshold: 50,
                elements_selector: '.miracle-slider__slide:nth-of-type(-n+3) img',
            });
        }
    });
}
catch (e) {
    console.info( 'sorry, you have is error in miracleSlider' );
    console.info( e );
}

let bgLazy = new LazyLoad({
    elements_selector: '.sertificate',
    threshold: 50
});
