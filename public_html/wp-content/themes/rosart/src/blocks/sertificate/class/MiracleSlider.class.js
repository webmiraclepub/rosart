class MiracleSlider {

	constructor( item, options ) {
		if ( item != '' ){
			this.elem = item;
			this.position = 1;
			this.imgOnSlide = options.images;
			this.modal = options.modal || false;
			this.slides = item.querySelectorAll('img').length;

			this.setDots();
			this.setContent();

			if ( this.modal ){
				this.listenOpen	();
			}

			this.listenNav();
			this.listenDots();
		} else {
			this.listenOpen	();
		}

	}

	listenOpen(){
		let self = this;
		document.querySelectorAll('.miracle-slider__slide').forEach((element, index, array)=>{
			element.addEventListener( 'click', (e)=>{
				let img = element.querySelector('img')
				this.createModal(img);
				self.listenClose();
			});
		});
	}

	createModal(img){
		let modalWindow = document.createElement( 'div' );
		modalWindow.classList.add( 'miracle-slider__slide_modal' );
		let modalimg = document.createElement( 'img' );
		modalimg.setAttribute( "src", img.getAttribute( 'src' ) );
		modalimg.setAttribute( "id", img.getAttribute( 'id' ) );
		modalWindow.appendChild(modalimg);
		document.body.appendChild(modalWindow);
		let modal = document.querySelector('.miracle-slider__slide_modal');
		setTimeout(function(){modal.classList.add('miracle-slider__slide_show');}, 200)
	}

	listenClose(){
		let modal = document.querySelector('.miracle-slider__slide_modal');
		modal.addEventListener( 'click', (e)=>{
			if (e.target.classList.contains('miracle-slider__slide_modal') ){
				modal.classList.remove('miracle-slider__slide_show');
				setTimeout(function(){ document.body.removeChild( modal ); }, 400);
			}
		});
	}

	setDots(){
		let dots = document.createElement('div');
		dots.classList.add('miracle-slider__nav');

		let j = 1;
		for (let i = 1; i <= this.slides; i++) {
			if( i%this.imgOnSlide == 1 ){
				let dot = document.createElement('button');
				dot.classList.add('miracle-slider__dot');
				dot.setAttribute( 'type', 'button' );
				dot.setAttribute( 'data-slide', j );
				dots.appendChild(dot);
				j++;
			}
		}
		dots.querySelector('button:nth-of-type(1)').classList.add('miracle-slider__dot_active');
		this.elem.insertBefore(dots, this.elem.firstChild);
	}

	setContent(){
		let content = document.createElement('div');
		content.classList.add('miracle-slider__outer');
		let prev = document.createElement('button');
		prev.classList.add('miracle-slider__prev');
		prev.setAttribute('type', 'button');
		prev.innerHTML = '<i class="miracle-font-chevron-left">';
		let outline = this.createOutline();
		let next = document.createElement('button');
		next.classList.add('miracle-slider__next');
		next.setAttribute('type', 'button');
		next.innerHTML = '<i class="miracle-font-chevron-right">';

		content.appendChild(prev);
		content.appendChild(outline);
		content.appendChild(next);

		this.elem.appendChild( content );
		this.elem.querySelector('.miracle-slider__stage').style.transform = 'translateX( 0px )';
	}

	createOutline(){
		let outline = document.createElement('div');
		outline.classList.add('miracle-slider__content');
		let stage = document.createElement('div');
		stage.classList.add('miracle-slider__stage');

		let images = this.elem.querySelectorAll('img');
		for (let i = 0; i < this.slides; i++) {
			let slide = document.createElement('div');
			slide.classList.add('miracle-slider__slide');
			let img = images[i].cloneNode();
			slide.appendChild(img);

			if( this.modal ){
				let icon = document.createElement('i');
				icon.classList.add('miracle-font-zoom');
				icon.classList.add('miracle-slider__icon-zoom');
				slide.appendChild( icon );
			}

			stage.appendChild( slide );
			images[i].parentNode.removeChild(images[i]);
		}

		outline.appendChild(stage);

		return outline;
	}

	listenNav(){
		let self = this;
		self.elem.addEventListener( 'click', (e)=>{
			if( e.target.classList.contains('miracle-slider__next') || e.target.parentNode.classList.contains('miracle-slider__next') ){
				this.moveNext();
			}else if( e.target.classList.contains('miracle-slider__prev') || e.target.parentNode.classList.contains('miracle-slider__prev') ){
				this.movePrev();
			}
		});
	}

	listenDots(){
		let self = this;
		self.elem.addEventListener( 'click', (e)=>{
			if( e.target.classList.contains('miracle-slider__dot') && !e.target.classList.contains('miracle-slider__dot_active') ){
				let slide = e.target.getAttribute( 'data-slide' );
				slide = ( parseInt(slide) - 1 ) * this.imgOnSlide + 1;
				this.Move( slide );
				this.position = slide;
			}
		});
	}

	movePrev(){
		if( this.position > 1 ){
			let move = this.position - 1;
			this.Move( move );
			this.position--;
		}
	}

	moveNext(){
		if( this.position < this.slides ){
			let move = this.position + 1;
			this.Move( move );
			this.position++;
		}
	}

	Move( move ){
		let m = 0 - this.elem.querySelector('.miracle-slider__slide:nth-of-type(' + move + ')').offsetLeft;
		this.elem.querySelector('.miracle-slider__stage').style.transform = 'translateX( ' + m + 'px )';

		if( move%this.imgOnSlide == 1 ){
			let dotNum = ( ( move - 1 ) / this.imgOnSlide ) + 1;
			this.elem.querySelector('.miracle-slider__dot_active[data-slide]').classList.remove('miracle-slider__dot_active');
			this.elem.querySelector('.miracle-slider__dot[data-slide="' + dotNum + '"]').classList.add('miracle-slider__dot_active');
		}
	}
}

export {MiracleSlider}
