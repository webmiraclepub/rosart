class ArchiveFilters{
	constructor(filters){
		this.filters = filters;

		this.listenLocation();

		this.initFilterClicks();
	}

	initFilterClicks(){
		let self = this;
		// this.filters.forEach((element, index, array)=>{
		// 	element.addEventListener('click', function(e){
		// 		e.preventDefault();
		// 		array.forEach((element, index, array)=>{
		// 			if ( element.classList.contains('archive-filter__filters_active') )
		// 				element.classList.remove('archive-filter__filters_active');
		// 		})
		// 		element.classList.add('archive-filter__filters_active');
		// 		self.getFilteredProjects(element);
		// 	});
		// });
	}

	listenLocation(){
		let term_slug = location.search.match( /[^=]*/ig )[2];

		if( term_slug ){
			this.filters.forEach( (elem, index) => {
				if( elem.getAttribute( 'data-term-slug' ) == term_slug ){
					elem.classList.add('archive-filter__filters_active');
				}else{
					elem.classList.remove('archive-filter__filters_active');
				}
			});
		}
	}

	getFilteredProjects(element){
		// ajax heres

		// let slug = element.getAttribute( 'data-term-slug' );
		// location.href = '?slug=' + slug;
		// location.hash = '?slug=' + slug;
	}
}

export {ArchiveFilters};
