import {ArchiveFilters} from './class/ArchiveFilters.class';
require('./style/components.scss');
try{
	document.addEventListener('DOMContentLoaded', function(){
		let filterslist = document.querySelectorAll('.archive-filter__filters');
		let filterObj = new ArchiveFilters(filterslist);
	});
}
catch(e){
	console.error(e);
}
