import LazyLoad from 'vanilla-lazyload/src/lazyload.js';
import {MiracleSlider} from '../sertificate/class/MiracleSlider.class.js';
require('./style/component.scss');

try {
    document.addEventListener( 'DOMContentLoaded', ()=>{
        let slides = document.querySelectorAll('.miracle-slider.support__slider');
        for (var i = 0; i < slides.length; i++) {
            let options = {
                images: 3
            }
            let slideObj = new MiracleSlider( slides[i], options );

            let lazy = new LazyLoad({
                container: slides[i].querySelector('.miracle-slider__content'),
                elements_selector: '.miracle-slider__slide:nth-of-type(n+5) img',
            });
            let lazyActiveImage = new LazyLoad({
                threshold: 50,
                elements_selector: '.miracle-slider__slide:nth-of-type(-n+4) img',
            });
        }
    });
}
catch (e) {
    console.info( 'sorry, you have is error in miracleSlider' );
    console.info( e );
}

let bgLazy = new LazyLoad({
    elements_selector: '.support',
    threshold: 50
});
