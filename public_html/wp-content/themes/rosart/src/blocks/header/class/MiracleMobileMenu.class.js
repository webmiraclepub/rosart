class MiracleMobileMenu {
    constructor(item) {
        this.elem = item;
        this.active = false;

        this.listenMenu();
        this.listenClose();
    }

    listenMenu(){
        let actions = document.querySelectorAll( '[data-action="mobile-menu"]' );
        let self = this;

        for (var i = 0; i < actions.length; i++) {
            actions[i].addEventListener( 'click', (e)=>{
                self.active = true;
                self.toggle();
            });
        }
    }

    listenClose(){
        let close = this.elem.querySelector('.mobile-menu__close-button');
        let self = this;

        close.addEventListener( 'click', (e)=>{
            self.active = false;
            self.toggle();
        });
    }

    toggle(){
        if( this.active ){
            this.elem.classList.add('mobile-menu_active');
        }else{
            this.elem.classList.remove('mobile-menu_active');
        }
    }
}

export {MiracleMobileMenu}
