import { MiracleModal } from '../content-block/class/MiracleModal.class.js';
import { MiracleMobileMenu } from './class/MiracleMobileMenu.class.js';
import LazyLoad from 'vanilla-lazyload/src/lazyload.js';
require('./style/component.scss');

try{
    let menu = document.querySelector('.header__top-menu');
    let prevMenu = menu.previousElementSibling;
    document.addEventListener( 'scroll', (e)=>{
        let scrollTop = window.pageYOffset || document.documentElement.scrollTop;
        if( scrollTop > 500 && !menu.classList.contains('top-menu_scroll') ){
            prevMenu.style.paddingBottom = menu.offsetHeight + 'px';
            menu.classList.add('top-menu_scroll');
        }else if( scrollTop < 500 && menu.classList.contains('top-menu_scroll') ){
            menu.classList.add('top-menu_hide');
            setTimeout( ()=>{
                menu.classList.remove('top-menu_hide');
                menu.classList.remove('top-menu_scroll');
                prevMenu.style.paddingBottom = '0px';
            }, 500);
        }
    });

    let modal = document.querySelector('.firm-contacts__link[data-modal]');
    let ModalObj = new MiracleModal( modal );

    let mobileMenu = document.querySelectorAll('.mobile-menu');

    if( mobileMenu.length > 0 ){
        let mobileMenuObj = new MiracleMobileMenu( mobileMenu[0] );
    }

    let bgLazy = new LazyLoad({
        elements_selector: '.logotype__image',
        threshold: 50
    });

}catch(e){
    console.info(e);
}
