import {MiracleSlider} from '../sertificate/class/MiracleSlider.class.js';
require('./style/components.scss');

try {
    document.addEventListener( 'DOMContentLoaded', ()=>{
        let options = {
            modal: true
        }
        let slideObj = new MiracleSlider( '', options );

    });
}
catch (e) {
    console.info( e );
}
