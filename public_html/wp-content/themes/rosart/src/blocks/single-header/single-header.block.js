import LazyLoad from 'vanilla-lazyload/src/lazyload.js';
require('./style/component.scss');

try{
    document.addEventListener( 'DOMContentLoaded', (e)=>{
        let top = document.querySelector('.header').offsetHeight + 10;
        let headerTitle = ( document.querySelector('.single-header').parentNode != document.body ) ? document.querySelector('.single-header').parentNode : document.querySelector('.single-header');
        let headerTitleContent = document.querySelector('.single-header__content');

        headerTitle.style.top = '-' + top + 'px';
        headerTitle.style.marginBottom = '-' + top + 'px';
        headerTitleContent.style.paddingTop = ( top + 55 ) + 'px';

        let bgLazy = new LazyLoad({
            elements_selector: '.single-header',
            threshold: 50
        });
    });
}catch(e){
    console.info(e);
}
