import { MiracleModal } from '../content-block/class/MiracleModal.class.js';
import LazyLoad from 'vanilla-lazyload/src/lazyload.js';
require('./style/component.scss');

document.addEventListener( 'DOMContentLoaded', ()=>{
    try {
        let services = document.querySelectorAll('.our-services');

        for (var i = 0; i < services.length; i++) {

            let title = services[i].querySelectorAll('.our-services__title');
            let max_title = 0;
            for (var n = 0; n < title.length; n++) {
                max_title = ( max_title > title[n].offsetHeight ) ? max_title : title[n].offsetHeight;
            }

            let content = services[i].querySelectorAll('.our-services__content');
            let max_content = 0;
            for (var x = 0; x < content.length; x++) {
                max_content = ( max_content > content[x].offsetHeight ) ? max_content : content[x].offsetHeight;
            }

            let block = services[i].querySelectorAll('.our-services__block');
            for (let j = 0; j < block.length; j++) {
                block[j].querySelector('.our-services__title').style.height = max_title + 'px';
                block[j].querySelector('.our-services__content').style.height = max_content + 'px';
            }
        }

        let bgLazy = new LazyLoad({
            elements_selector: '.how-work',
            threshold: 50
        });

        let imgLazy = new LazyLoad({
            elements_selector: 'img.how-work__image',
            threshold: 50
        });

        let modal = document.querySelectorAll('.how-work__button[data-modal]');

        for (let i = 0; i < modal.length; i++) {
            let obj = new MiracleModal( modal[i] );
        }
    } catch (e) {
        console.info(e);
    }
});
