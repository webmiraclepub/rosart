import LazyLoad from 'vanilla-lazyload/src/lazyload.js';

class MiracleModal {

    constructor(item, options){
        this.slug = item.getAttribute( 'data-modal' );
        this.elem = document.querySelector(`.miracle-modal[data-modal="${this.slug}"]`);
        this.elem.style.display = "none";
        this.active = false;
        this.lazy = true;
        this.switch = item;

        this.listenSwitch();
        this.listenClose();
    }

    action(){
        if( this.lazy ){
            let bgLazy = new LazyLoad({
                elements_selector: `.miracle-modal[data-modal="${this.slug}"]`
            });
        }

        if( !this.active ){
            this.elem.style.display = "";
            setTimeout( ()=>{
                document.body.style.overflow = "hidden";
                let modal = document.querySelector('.modal');
                modal.style.zIndex = 1000;
                modal.classList.add('modal_active');

                this.elem.classList.add('miracle-modal_active');
                this.elem.style.zIndex = 1001;
                this.active = true;
            }, 50);
        }
    }

    deaction(){
        if( this.active ){
            document.body.style.overflow = "";
            let modal = document.querySelector('.modal');
            modal.classList.remove('modal_active');

            this.elem.classList.remove('miracle-modal_active');
            setTimeout( ()=>{
                    modal.style.zIndex = -1;
                    this.elem.style.zIndex = -2;
                    this.elem.style.display = "none";
                }, 600 );
            this.active = false;
        }
    }

    listenClose(){
        let modal = document.querySelector('.modal');
        let self = this;
        modal.addEventListener( 'click', (e)=>{
            let boll = e.target.classList.contains( 'modal_active' ) || e.target.classList.contains( 'miracle-modal__close-button' );

            if( boll && self.active ){
                self.deaction();
            }
        });
    }

    listenSwitch(){
        let self = this;
        let sw = this.switch;
        sw.addEventListener( 'click', (e)=>{
            if( !self.active ){
                self.action();
            }
            return false;
        });
    }

}

export {MiracleModal}
