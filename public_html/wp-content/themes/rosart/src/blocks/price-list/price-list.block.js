import { MiracleModal } from '../content-block/class/MiracleModal.class.js';
import { MiracleTab } from './class/MiracleTab.class.js';
import LazyLoad from 'vanilla-lazyload/src/lazyload.js';
require('./style/component.scss');

try {
    let w = document.body.offsetWidth;

    if( w > 1200 ){
        let getTemplate = new Promise((resolve, reject) => {
            let root = document.querySelector('.price-list .price-list__content');
            let template = document.querySelector('template[type="price-list-large"]').content.cloneNode(true);
            root.appendChild(template);

            resolve();
        });

        getTemplate.then(
            result => {

                document.addEventListener( 'DOMContentLoaded', ()=>{
                    let card = document.querySelectorAll('.price-list-card-content');
                    for (var i = 0; i < card.length; i++) {

                        let title = card[i].querySelectorAll('.price-list-card-content__title');
                        let max_title = 0;
                        for (var n = 0; n < title.length; n++) {
                            max_title = ( max_title > title[n].offsetHeight ) ? max_title : title[n].offsetHeight;
                        }

                        let content = card[i].querySelectorAll('.price-list-card-content__service-list');
                        let max_content = 0;
                        for (var x = 0; x < content.length; x++) {
                            max_content = ( max_content > content[x].offsetHeight ) ? max_content : content[x].offsetHeight;
                        }

                        let block = card[i].querySelectorAll('.price-list-card-content__block');
                        for (let j = 0; j < block.length; j++) {
                            block[j].querySelector('.price-list-card-content__title').style.height = max_title + 'px';
                            block[j].querySelector('.price-list-card-content__service-list').style.height = max_content + 'px';
                        }
                    }
                });

                let bgLazy = new LazyLoad({
                    elements_selector: '.price-list',
                    threshold: 50
                });

                let modal = document.querySelectorAll('.price-list-card-content__button[data-modal]');

                for (let i = 0; i < modal.length; i++) {
                    let obj = new MiracleModal( modal[i] );
                }
            }, error => {
                console.info(error);
        });
    } else {
        let getTemplate = new Promise((resolve, reject) => {
            let root = document.querySelector('.price-list .price-list__content');
            let template = document.querySelector('template[type="price-list-medium"]').content.cloneNode(true);
            root.appendChild(template);

            resolve();
        });

        getTemplate.then(
            result => {
                let tabs = document.querySelectorAll('.price-list-tab__block');
                for (let i = 0; i < tabs.length; i++) {
                    let tabObj = new MiracleTab( tabs[i] );

                    try {
                        let modalObj = new MiracleModal( tabs[i].querySelector('[data-modal]') );
                    } catch (e) {
                        // Dont has modal
                    }
                }

                let bgLazy = new LazyLoad({
                    elements_selector: '.price-list',
                    threshold: 50
                });
            }, error => {
                console.info(error);
        });
    }
} catch (e) {
    console.info(e);
}
