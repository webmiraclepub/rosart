class MiracleTab {
    constructor(item) {
        this.elem = item;
        this.height = item.clientHeight;
        this.active = ( item.getAttribute('data-active') != undefined && item.getAttribute('data-active') == 'true' ) ? true : false;

        this.toggle();
        this.listenClick();
    }

    toggle(){
        if( this.active ){
            this.elem.classList.add('price-list-tab__block_active');
            this.elem.querySelector('.price-list-tab__content').style.height = this.height + 'px';
        }else{
            this.elem.classList.remove('price-list-tab__block_active');
            this.elem.querySelector('.price-list-tab__content').style.height = '0px';
        }
    }

    listenClick(){
        let self = this;

        this.elem.querySelector('.price-list-tab__button').addEventListener( 'click', ()=>{
            self.active = !self.active;
            self.toggle();
        });
    }
}

export { MiracleTab }
