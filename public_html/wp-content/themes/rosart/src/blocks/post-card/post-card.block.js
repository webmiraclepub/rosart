require('./style/component.scss');
import LazyLoad from 'vanilla-lazyload/src/lazyload.js';


try {
    document.addEventListener( 'DOMContentLoaded', ()=>{
        let bgLazy = new LazyLoad({
            elements_selector: '.post-card',
            threshold: 50
        });

        let title = document.querySelectorAll('.post-card__title');
        let max_title = 0;
        for (let n = 0; n < title.length; n++) {
            max_title = ( max_title > title[n].offsetHeight ) ? max_title : title[n].offsetHeight;
        }

        let content = document.querySelectorAll('.post-card__description');
        let max_content = 0;
        for (let x = 0; x < content.length; x++) {
            max_content = ( max_content > content[x].offsetHeight ) ? max_content : content[x].offsetHeight;
        }

        let block = document.querySelectorAll('.post-card');
        for (let j = 0; j < block.length; j++) {
            block[j].querySelector('.post-card__title').style.height = max_title + 'px';
            block[j].querySelector('.post-card__description').style.height = max_content + 'px';
        }
    });
} catch (e) {
    console.info( e );
}
