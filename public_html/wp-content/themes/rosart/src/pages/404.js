var normalize = require('normalize-css');

//Blocks
import '../globals/remSettings.scss';
import '../blocks/content-block/content-block.block.js';

import '../blocks/header/header.block.js';
import '../blocks/how-work/how-work.block.js';
import '../blocks/body-404/body-404.block.js';
import '../blocks/bottom-form/bottom-form.block.js';
import '../blocks/footer/footer.block.js';

document.addEventListener( 'DOMContentLoaded', ()=>{
    var fonts = require('../globals/font/font.scss');
});
