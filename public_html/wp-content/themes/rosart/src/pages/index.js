var normalize = require('normalize-css');
var fonts = require('../globals/animate.scss');

//Blocks
import '../globals/remSettings.scss';
import '../blocks/header/header.block.js';
import '../blocks/about-us/about-us.block.js';
import '../blocks/content-block/content-block.block.js';
import '../blocks/how-work/how-work.block.js';
import '../blocks/send-requery/send-requery.block.js';
import '../blocks/choose-us/choose-us.block.js';
import '../blocks/post-develop-card/post-develop-card.block.js';
import '../blocks/last-portfolio/last-portfolio.block.js';
import '../blocks/price-list/price-list.block.js';
import '../blocks/work-step/work-step.block.js';
import '../blocks/sertificate/sertificate.block.js';
import '../blocks/support/support.block.js';
import '../blocks/yandex-map/yandex-map.block.js';
import '../blocks/bottom-form/bottom-form.block.js';
import '../blocks/footer/footer.block.js';

// header-slider
import '../blocks/header-slider/header-slider.block.js';

document.addEventListener( 'DOMContentLoaded', ()=>{
    var fonts = require('../globals/font/font.scss');
});


import { WOW } from 'wowjs';
var wow = new WOW(
    {
        boxClass:     'miracle-wow',      // animated element css class (default is wow)
        animateClass: 'animated', // animation css class (default is animated)
        offset:       0,          // distance to the element when triggering the animation (default is 0)
        mobile:       true,       // trigger animations on mobile devices (default is true)
        live:         false,       // act on asynchronously loaded content (default is true)
        callback:     function(box) {
          // the callback is fired every time an animation is started
          // the argument that is passed in is the DOM node being animated
        },
        scrollContainer: null // optional scroll container selector, otherwise use window
    }
);
wow.init();
