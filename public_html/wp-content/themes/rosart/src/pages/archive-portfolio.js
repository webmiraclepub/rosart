var normalize = require('normalize-css');

//Blocks
import '../globals/remSettings.scss';
import '../blocks/content-block/content-block.block.js';

import '../blocks/header/header.block.js';
import '../blocks/about-us/about-us.block.js';
import '../blocks/single-header/single-header.block.js';
import '../blocks/archive-filter/archive-filter.block.js';
import '../blocks/home-body/home-body.block.js';
import '../blocks/post-develop-card/post-develop-card.block.js';
import '../blocks/sertificate/sertificate.block.js';
import '../blocks/support/support.block.js';
import '../blocks/bottom-form/bottom-form.block.js';
import '../blocks/footer/footer.block.js';

document.addEventListener( 'DOMContentLoaded', ()=>{
    var fonts = require('../globals/font/font.scss');
});