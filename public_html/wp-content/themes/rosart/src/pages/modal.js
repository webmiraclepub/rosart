class MiracleModal {
    constructor( element, options = {}) {
        this.elem = element;
        this.listenClick();

        return this;
    }

    listenClick(){
        let self = this;
        this.elem.addEventListener( 'click', (event)=>{
            event.preventDefault();
            console.log( self.elem );
        });
    }
}
