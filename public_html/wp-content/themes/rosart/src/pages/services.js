var normalize = require('normalize-css');

//Blocks
import '../globals/remSettings.scss';
import '../blocks/content-block/content-block.block.js';

import '../blocks/header/header.block.js';
import '../blocks/about-us/about-us.block.js';
import '../blocks/single-header/single-header.block.js';
import '../blocks/single-body/single-body.block.js';
import '../blocks/sertificate/sertificate.block.js';
import '../blocks/support/support.block.js';
import '../blocks/post-card/post-card.block.js';
import '../blocks/bottom-form/bottom-form.block.js';
import '../blocks/footer/footer.block.js';
import '../blocks/service-list/service-list.block.js';

document.addEventListener( 'DOMContentLoaded', ()=>{
    var fonts = require('../globals/font/font.scss');
});