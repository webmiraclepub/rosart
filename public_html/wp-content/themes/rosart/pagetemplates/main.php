<?php
/*
Template Name: Шаблон "Главная страница"
*/

get_header();

$theme_dir = get_template_directory( ) . '/views_support/';
$theme_dir_uri = get_template_directory_uri( );
$bgi = get_field( 'miracle-global-header-bg', 'option' );
$bgi_full = $bgi['url'];
$bgi_lazy = $bgi['sizes']['lazy'];
$logo_src = get_field( 'miracle-global-logo-image', 'option' )['url'];
$logo_alt = get_field( 'miracle-global-logo-image', 'option' )['alt'];
$logo_title = get_field( 'miracle-global-logo-image', 'option' )['title'];
$title = get_field( 'miracle-main-header-title' );
$subtitle = get_field( 'miracle-main-header-subtitle' );
include( $theme_dir . 'main/header-slider.php' );

$bgi = get_field( 'miracle-main-about-us-1-bg' );
$bgi_full = $bgi['url'];
$bgi_lazy = $bgi['sizes']['lazy'];
$image = get_field( 'miracle-main-about-us-1-image' );
$image_lazy = $image['sizes']['lazy'];
$image_full = $image['url'];
$image_alt = $image['alt'];
$image_title = $image['title'];
$title = get_field( 'miracle-main-about-us-1-title' );
$subtitle = get_field( 'miracle-main-about-us-1-subtitle' );
$content = get_field( 'miracle-main-about-us-1-content' );
$info_grid = miracle_get_about_us_info_grid();
include( $theme_dir . 'main/about-us-1.php' );

$posts = get_posts( array( 'post_type' => 'portfolio', 'numberposts' => 4, 'orderby' => 'date' ) );
$main_last_portfolio = miracle_get_main_last_portfolio( $posts );
$portfolio_link = get_post_type_archive_link( 'portfolio' );
include( $theme_dir . 'main/last-portfolio.php' );

$title = get_field( 'miracle-main-full-cycle-work-title' );
$content = get_field( 'miracle-main-full-cycle-work-content' );
$bgi = get_field( 'miracle-main-full-cycle-work-bg' );
$bgi_lazy = $bgi['sizes']['lazy'];
$bgi_full = $bgi['url'];
$image = get_field( 'miracle-main-full-cycle-work-image' );
$image_lazy = $image['sizes']['lazy'];
$image_full = $image['url'];
$image_alt = $image['alt'];
$image_title = $image['title'];
$service = miracle_get_how_work_our_services();
include( $theme_dir . 'main/full-cycle-work.php' );

$title = get_field( 'miracle-main-send-requery-1-title' );
$content = get_field( 'miracle-main-send-requery-1-content' );
$bgi = get_field( 'miracle-main-send-requery-1-bg' );
$bgi_full = $bgi['url'];
$bgi_lazy = $bgi['sizes']['lazy'];
include( $theme_dir . 'main/send-requery.php' );

$title = get_field( 'miracle-main-design-style-title' );
$subtitle = get_field( 'miracle-main-design-style-subtitle' );
$content = get_field( 'miracle-main-design-style-content' );
$bgi = get_field( 'miracle-main-design-style-bg' );
$bgi_lazy = $bgi['sizes']['lazy'];
$bgi_full = $bgi['url'];
$image = get_field( 'miracle-main-design-style-image' );
$image_lazy = $image['sizes']['lazy'];
$image_full = $image['url'];
$image_alt = $image['alt'];
$image_title = $image['title'];
include( $theme_dir . 'main/design-style.php' );

$title = get_field( 'miracle-main-design-interface-title' );
$subtitle = get_field( 'miracle-main-design-interface-subtitle' );
$content = get_field( 'miracle-main-design-interface-content' );
$bgi = get_field( 'miracle-main-design-interface-bg' );
$bgi_lazy = $bgi['sizes']['lazy'];
$bgi_full = $bgi['url'];
$image = get_field( 'miracle-main-design-interface-image' );
$image_lazy = $image['sizes']['lazy'];
$image_full = $image['url'];
$image_alt = $image['alt'];
$image_title = $image['title'];
include( $theme_dir . 'main/design-interface.php' );

$title = get_field( 'miracle-main-integrated-promotion-title' );
$subtitle = get_field( 'miracle-main-integrated-promotion-subtitle' );
$content = get_field( 'miracle-main-integrated-promotion-content' );
$bgi = get_field( 'miracle-main-integrated-promotion-bg' );
$bgi_lazy = $bgi['sizes']['lazy'];
$bgi_full = $bgi['url'];
$image = get_field( 'miracle-main-integrated-promotion-image' );
$image_lazy = $image['sizes']['lazy'];
$image_full = $image['url'];
$image_alt = $image['alt'];
$image_title = $image['title'];
$circle = miracle_get_about_us_circle_card();
include( $theme_dir . 'main/integrated-promotion.php' );

$title = get_field( 'miracle-main-choose-us-title' );
$bgi = get_field( 'miracle-main-choose-us-bg' );
$bgi_lazy = $bgi['sizes']['lazy'];
$bgi_full = $bgi['url'];
$choose_card = miracle_get_choose_cards();
include( $theme_dir . 'main/choose-us.php' );

$title = get_field( 'miracle-main-price-list-title' );
$bgi = get_field( 'miracle-main-price-list-bg' );
$bgi_lazy = $bgi['sizes']['lazy'];
$bgi_full = $bgi['url'];
$price_card = miracle_get_large_price_list_card();
$price_tab = miracle_get_large_price_list_card( 'tabs' );
include( $theme_dir . 'main/price-list.php' );

$title = get_field( 'miracle-main-work-step-title' );
$bgi = get_field( 'miracle-main-work-step-bg' );
$bgi_lazy = $bgi['sizes']['lazy'];
$bgi_full = $bgi['url'];
$step_card = miracle_get_work_step_cards();
include( $theme_dir . 'main/work-step.php' );

$title = get_field( 'miracle-main-request-call-title' );
$bgi = get_field( 'miracle-main-request-call-bg' );
$bgi_lazy = $bgi['sizes']['lazy'];
$bgi_full = $bgi['url'];
$content = get_field( 'miracle-main-request-call-content' );
include( $theme_dir . 'main/request-call.php' );

$title = get_field( 'miracle-global-sertificate-title', 'option' );
$bgi = get_field( 'miracle-global-sertificate-bg', 'option' );
$bgi_lazy = $bgi['sizes']['lazy'];
$bgi_full = $bgi['url'];
$gallery = get_field( 'miracle-global-sertificate-gallery', 'option' );
$slides = miracle_get_slide_images( $gallery );
include( $theme_dir . 'global/sertificate.php' );


$title = get_field( 'miracle-global-trust-title', 'option' );
$bgi = get_field( 'miracle-global-trust-bg', 'option' );
$bgi_lazy = $bgi['sizes']['lazy'];
$bgi_full = $bgi['url'];
$gallery = get_field( 'miracle-global-trust-gallery', 'option' );
$slides = miracle_get_slide_images( $gallery );
include( $theme_dir . 'global/parthner.php' );

$map = json_decode( get_field( 'miracle-global-yandex-map', 'option' ) );
$y = $map->marks[0]->coords[0];
$x = $map->marks[0]->coords[1];
$center_y = $map->center_lat;
$center_x = $map->center_lng;
$hint = $map->marks[0]->content;
$image = get_field( 'miracle-global-yandex-map-image', 'option' )['url'];
$ballon = get_field( 'miracle-global-yandex-map-ballon', 'option' );
include( $theme_dir . 'global/yandex-map.php' );

include( $theme_dir . 'global/bottom-form.php' );

$form1 = miracle_get_global_form( 'send-phone' );
$form2 = miracle_get_global_form( 'audit' );
$form3 = miracle_get_global_form( 'send-list' );
include( $theme_dir . 'main/modal.php' );

get_footer();

 ?>
