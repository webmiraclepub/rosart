<?php
/*
Template Name: Шаблон "Контакты"
*/

get_header();

$theme_dir = get_template_directory( ) . '/views_support/';
$theme_dir_uri = get_template_directory_uri( );

$bgi = get_field( 'miracle-global-header-bg', 'option' );
$bgi_full = $bgi['url'];
$bgi_lazy = $bgi['sizes']['lazy'];
$title = get_the_title();
$breadcrumbs = miracle_get_breadcrumbs( array( 'Главная' => get_home_url() ) );
$home_link = get_home_url();
include( $theme_dir . 'global/single-header.php' );

include( $theme_dir . 'contacts/contacts-body.php' );

$map = json_decode( get_field( 'miracle-global-yandex-map', 'option' ) );
$y = $map->marks[0]->coords[0];
$x = $map->marks[0]->coords[1];
$center_y = $map->center_lat;
$center_x = $map->center_lng;
$hint = $map->marks[0]->content;
$image = get_field( 'miracle-global-yandex-map-image', 'option' )['url'];
$ballon = get_field( 'miracle-global-yandex-map-ballon', 'option' );
include( $theme_dir . 'global/yandex-map.php' );
include( $theme_dir . 'global/bottom-form.php' );

$title = get_field( 'miracle-global-sertificate-title', 'option' );
$bgi = get_field( 'miracle-global-sertificate-bg', 'option' );
$bgi_lazy = $bgi['sizes']['lazy'];
$bgi_full = $bgi['url'];
$gallery = get_field( 'miracle-global-sertificate-gallery', 'option' );
$slides = miracle_get_slide_images( $gallery );
include( $theme_dir . 'global/sertificate.php' );

$title = get_field( 'miracle-global-trust-title', 'option' );
$bgi = get_field( 'miracle-global-trust-bg', 'option' );
$bgi_lazy = $bgi['sizes']['lazy'];
$bgi_full = $bgi['url'];
$gallery = get_field( 'miracle-global-trust-gallery', 'option' );
$slides = miracle_get_slide_images( $gallery );
include( $theme_dir . 'global/parthner.php' );

$form1 = miracle_get_global_form( 'send-phone' );
$form2 = '';//miracle_get_global_form( 'audit' );
$form3 = miracle_get_global_form( 'send-list' );
include( $theme_dir . 'main/modal.php' );

get_footer();

 ?>
